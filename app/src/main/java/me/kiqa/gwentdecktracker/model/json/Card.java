package me.kiqa.gwentdecktracker.model.json;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import me.kiqa.gwentdecktracker.constant.CardTypeEnum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "categories",
        "faction",
        "flavor",
        "group",
        "href",
        "info",
        "name",
        "positions",
        "strength",
        "uuid",
        "variations"
})
public class Card implements Serializable
{

    @JsonProperty("categories")
    private List<Category> categories = null;
    @JsonProperty("faction")
    private Faction faction;
    @JsonProperty("flavor")
    private String flavor;
    @JsonProperty("group")
    private Group group;
    @JsonProperty("href")
    private String href;
    @JsonProperty("info")
    private String info;
    @JsonProperty("name")
    private String name;
    @JsonProperty("positions")
    private List<String> positions = null;
    @JsonProperty("strength")
    private Integer strength;
    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("variations")
    private List<Variation> variations = null;
    @JsonIgnore
    private int quantity = 1;
    private final static long serialVersionUID = -7451019959774719922L;

    @JsonProperty("categories")
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonProperty("faction")
    public Faction getFaction() {
        return faction;
    }

    @JsonProperty("faction")
    public void setFaction(Faction faction) {
        this.faction = faction;
    }

    @JsonProperty("flavor")
    public String getFlavor() {
        return flavor;
    }

    @JsonProperty("flavor")
    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    @JsonProperty("group")
    public Group getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(Group group) {
        this.group = group;
        if (group != null){
            if (group.getName() != null && group.getName() == CardTypeEnum.BRONZE){
                this.quantity = 3;
            }
        }
    }

    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    @JsonProperty("info")
    public String getInfo() {
        return info;
    }

    @JsonProperty("info")
    public void setInfo(String info) {
        this.info = info;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("positions")
    public List<String> getPositions() {
        return positions;
    }

    @JsonProperty("positions")
    public void setPositions(List<String> positions) {
        this.positions = positions;
    }

    @JsonProperty("strength")
    public Integer getStrength() {
        return strength;
    }

    @JsonProperty("strength")
    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @JsonProperty("variations")
    public List<Variation> getVariations() {
        return variations;
    }

    @JsonProperty("variations")
    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void incrementQuantity(int amount){
        this.quantity += amount;
    };

}
package me.kiqa.gwentdecktracker.model.json;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "art",
        "availability",
        "craft",
        "href",
        "mill",
        "rarity",
        "uuid"
})
public class Variation implements Serializable
{

    @JsonProperty("art")
    private Art art;
    @JsonProperty("availability")
    private String availability;
    @JsonProperty("craft")
    private CostType craft;
    @JsonProperty("href")
    private String href;
    @JsonProperty("mill")
    private CostType mill;
    @JsonProperty("rarity")
    private Rarity rarity;
    @JsonProperty("uuid")
    private String uuid;
    private final static long serialVersionUID = 2049094492103968772L;

    @JsonProperty("art")
    public Art getArt() {
        return art;
    }

    @JsonProperty("art")
    public void setArt(Art art) {
        this.art = art;
    }

    @JsonProperty("availability")
    public String getAvailability() {
        return availability;
    }

    @JsonProperty("availability")
    public void setAvailability(String availability) {
        this.availability = availability;
    }

    @JsonProperty("craft")
    public CostType getCraft() {
        return craft;
    }

    @JsonProperty("craft")
    public void setCraft(CostType craft) {
        this.craft = craft;
    }

    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    @JsonProperty("mill")
    public CostType getMill() {
        return mill;
    }

    @JsonProperty("mill")
    public void setMill(CostType mill) {
        this.mill = mill;
    }

    @JsonProperty("rarity")
    public Rarity getRarity() {
        return rarity;
    }

    @JsonProperty("rarity")
    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
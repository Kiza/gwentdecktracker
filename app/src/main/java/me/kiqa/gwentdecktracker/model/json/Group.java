package me.kiqa.gwentdecktracker.model.json;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import me.kiqa.gwentdecktracker.constant.CardTypeEnum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "href",
        "name"
})
public class Group implements Serializable
{

    @JsonProperty("href")
    private String href;
    @JsonProperty("name")
    private CardTypeEnum name;
    private final static long serialVersionUID = -7799215929473331741L;

    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    @JsonProperty("name")
    public CardTypeEnum getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(CardTypeEnum name) {
        this.name = name;
    }

}
package me.kiqa.gwentdecktracker.model.json;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "artist",
        "fullsizeImage",
        "mediumsizeImage",
        "thumbnailImage"
})
public class Art implements Serializable
{

    @JsonProperty("artist")
    private String artist;
    @JsonProperty("fullsizeImage")
    private String fullsizeImage;
    @JsonProperty("mediumsizeImage")
    private String mediumsizeImage;
    @JsonProperty("thumbnailImage")
    private String thumbnailImage;
    private final static long serialVersionUID = 1619790141208327825L;

    @JsonProperty("artist")
    public String getArtist() {
        return artist;
    }

    @JsonProperty("artist")
    public void setArtist(String artist) {
        this.artist = artist;
    }

    @JsonProperty("fullsizeImage")
    public String getFullsizeImage() {
        return fullsizeImage;
    }

    @JsonProperty("fullsizeImage")
    public void setFullsizeImage(String fullsizeImage) {
        this.fullsizeImage = fullsizeImage;
    }

    @JsonProperty("mediumsizeImage")
    public String getMediumsizeImage() {
        return mediumsizeImage;
    }

    @JsonProperty("mediumsizeImage")
    public void setMediumsizeImage(String mediumsizeImage) {
        this.mediumsizeImage = mediumsizeImage;
    }

    @JsonProperty("thumbnailImage")
    public String getThumbnailImage() {
        return thumbnailImage;
    }

    @JsonProperty("thumbnailImage")
    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

}
package me.kiqa.gwentdecktracker.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.kiqa.gwentdecktracker.constant.CardListTypeEnum;
import me.kiqa.gwentdecktracker.model.json.Card;
import me.kiqa.gwentdecktracker.util.CardComparator;

/**
 * Created by ben on 21/9/17.
 */

public class CardList {
    private List<Card> cardList;
    private String name;
    private CardListTypeEnum listType;
    private Comparator<Card> comparator;

    public CardList(CardListTypeEnum listType, String name) {
        this.listType = listType;
        this.name = name;
        this.cardList = new ArrayList<>();
        this.comparator = new CardComparator();
    }

    public List<Card> getCardList() {
        return cardList;
    }

    public void setCardList(List<Card> cardList) {
        this.cardList = cardList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CardListTypeEnum getListType() {
        return listType;
    }

    public void setListType(CardListTypeEnum listType) {
        this.listType = listType;
    }

    public void sortList(){
        sortList(this.comparator);
    }

    public void sortList(Comparator<Card> comparator){
        Collections.sort(this.cardList,comparator);
    }
}

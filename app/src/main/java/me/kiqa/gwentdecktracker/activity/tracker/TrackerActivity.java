package me.kiqa.gwentdecktracker.activity.tracker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.kiqa.gwentdecktracker.R;
import me.kiqa.gwentdecktracker.activity.common.BaseCardListAdapter;
import me.kiqa.gwentdecktracker.constant.CardListTypeEnum;
import me.kiqa.gwentdecktracker.model.CardList;
import me.kiqa.gwentdecktracker.model.json.Card;
import me.kiqa.gwentdecktracker.util.CardComparator;

public class TrackerActivity extends AppCompatActivity{
    private CardList handCardList = new CardList(CardListTypeEnum.HAND, "Hand");
    private CardList boardCardList = new CardList(CardListTypeEnum.BOARD, "Board");
    private CardList graveyardCardList = new CardList(CardListTypeEnum.GRAVEYARD, "Graveyard");
    private CardList deckCardList = new CardList(CardListTypeEnum.DECK, "Deck");
    private boolean stubbed = true;
    private BaseCardListAdapter adapter = null;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        toolbar = (Toolbar) findViewById(R.id.trackerToolbar);
        setSupportActionBar(toolbar);

        // TODO: Remove this eventually when sql and rest calls populate data
        // stubbing out for testing
        if (stubbed) {
            deckCardList.setCardList(populateStubList("stub_detail_deck_15"));
            /*
                graveyardCardList = populateStubList("stub_detail_deck_5");
                boardCardList = populateStubList("stub_detail_deck_15");
                handCardList = populateStubList("stub_detail_deck_5");
            */
        }

        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new TrackerCardListAdapter(this, deckCardList.getCardList());
        setToolbarTitle("Deck");
        listView.setAdapter(adapter);
    }

    // Binds menu to app toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.tracker_menu,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch(menuItem.getItemId()){
            case R.id.trackerMenuReset :
                resetToDeck();
                break;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
     return true;
    }

    private void resetToDeck() {
        transferAllCardsFromList(handCardList,deckCardList);
        transferAllCardsFromList(graveyardCardList,deckCardList);
        transferAllCardsFromList(boardCardList,deckCardList);
        setToolbarTitle("Deck");
        deckCardList.sortList();
        adapter.setCardList(deckCardList.getCardList());
        adapter.notifyDataSetChanged();
    }

    private void transferAllCardsFromList(CardList from, CardList to){
        while (from.getCardList().size() > 0){
            to.getCardList().add(from.getCardList().remove(0));
        }
    }

    public void onListTypeNavClick(View view){
        //Log.i("TrackerClick: ",view.getTag().toString());
        switch (CardListTypeEnum.valueOf(view.getTag().toString())) {
            case BOARD : {
                adapter.setCardList(boardCardList.getCardList());
                setToolbarTitle("Board");
                break;
            }
            case DECK : {
                adapter.setCardList(deckCardList.getCardList());
                setToolbarTitle("Deck");
                break;
            }
            case GRAVEYARD : {
                adapter.setCardList(graveyardCardList.getCardList());
                setToolbarTitle("Graveyard");
                break;
            }
            case HAND : {
                adapter.setCardList(handCardList.getCardList());
                setToolbarTitle("Hand");
                break;
            }
            default : {
                adapter.setCardList(deckCardList.getCardList());
                setToolbarTitle("Deck");
                break;
            }
        }
        adapter.notifyDataSetChanged();
    }

    private List<Card> populateStubList(String stubDeckName){
        Comparator<Card> comparator = new CardComparator();
        List<Card> cardList = null;
        // set up an input stream on the stubbed file in res/raw/stub_deck_30.jsonn
        Log.i("TrackerActivity","Trying to read file: " + stubDeckName);
        InputStream inputStream = getResources().openRawResource(
                getResources().getIdentifier(stubDeckName,"raw", getPackageName()));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder deckStubJson = new StringBuilder();
        String line;
        // try reading the stub file
        try {
            while ((line = bufferedReader.readLine()) != null) {
                deckStubJson.append(line).append('\n');
            }
            Log.i("TrackerActivity","stub read into memory");

            ObjectMapper mapper = new ObjectMapper();
            Log.i("TrackerActivity","mapping to decklist");
            cardList = mapper.readValue(deckStubJson.toString(), new TypeReference<List<Card>>(){});
        } catch (Exception e){
            System.out.print("ERROR: Reading File.  " + e.getMessage());
        }
        Collections.sort(cardList,comparator);
        return cardList;
    }

    public CardList getHandCardList() {
        return handCardList;
    }

    public void setHandCardList(CardList handCardList) {
        this.handCardList = handCardList;
    }

    public CardList getBoardCardList() {
        return boardCardList;
    }

    public void setBoardCardList(CardList boardCardList) {
        this.boardCardList = boardCardList;
    }

    public CardList getGraveyardCardList() {
        return graveyardCardList;
    }

    public void setGraveyardCardList(CardList graveyardCardList) {
        this.graveyardCardList = graveyardCardList;
    }

    public CardList getDeckCardList() {
        return deckCardList;
    }

    public void setDeckCardList(CardList deckCardList) {
        this.deckCardList = deckCardList;
    }

    public BaseCardListAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(BaseCardListAdapter adapter) {
        this.adapter = adapter;
    }

    public void setToolbarTitle(String str){
        toolbar.setTitle("Tracker - " + str);
    }
}

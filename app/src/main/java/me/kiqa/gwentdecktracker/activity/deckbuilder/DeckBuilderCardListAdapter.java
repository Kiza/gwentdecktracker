package me.kiqa.gwentdecktracker.activity.deckbuilder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import me.kiqa.gwentdecktracker.R;
import me.kiqa.gwentdecktracker.activity.common.BaseCardListAdapter;
import me.kiqa.gwentdecktracker.model.json.Card;

/**
 * Created by ben on 21/9/17.
 */

public class DeckBuilderCardListAdapter extends BaseCardListAdapter {

    public DeckBuilderCardListAdapter(Context context, List<Card> cardList) {
        super(context, cardList);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // get the custom view from resources and inflate it
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.card_list_row, parent, false);
        // get the card at this position
        //Log.i("CardAdapter", "getting item via view: " + position);
        Card card = cardList.get(position);

        // set up the fields in the view
        TextView colourStripe = (TextView) rowView.findViewById(R.id.rowColourStripe);
        TextView strength = (TextView) rowView.findViewById(R.id.rowStrength);
        TextView title = (TextView) rowView.findViewById(R.id.rowTitle);
        TextView info = (TextView) rowView.findViewById(R.id.rowInfo);
        Button addButton = (Button) rowView.findViewById(R.id.rowLeftButton);
        Button removeButton = (Button) rowView.findViewById(R.id.rowMiddleButton);
        Button infoButton = (Button) rowView.findViewById(R.id.rowRightButton);

        // set the position in list to the button tag to identify
        // which row was clicked within the click handler
        addButton.setTag(position);
        removeButton.setTag(position);
        infoButton.setTag(position);

        // click handlers for each button
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeckBuilderActivity context = ((DeckBuilderActivity) view.getContext());
                Card selected = cardList.get((int) view.getTag());
                selected.incrementQuantity(-1);
                // if the card has run out of availability, remove from the list
                if (selected.getQuantity() == 0) {
                    cardList.remove((int) view.getTag());
                }
                /*context.getDeckList().add(selected);
                Collections.sort(context.getGraveyardCardList(),comparator);
                context.getAdapter().notifyDataSetChanged();*/
            }
        });


        // map values from card to row view
        colourStripe.setBackgroundResource(getColourForCard(card));
        strength.setText(card.getStrength() != null ? card.getStrength().toString() : "*");
        title.setText(card.getName());
        info.setText(card.getInfo() != null ? card.getInfo() : "");
        return rowView;
    }

}

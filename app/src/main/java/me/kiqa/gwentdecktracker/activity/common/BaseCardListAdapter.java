package me.kiqa.gwentdecktracker.activity.common;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.widget.BaseAdapter;

import java.util.Comparator;
import java.util.List;

import me.kiqa.gwentdecktracker.R;
import me.kiqa.gwentdecktracker.model.json.Card;
import me.kiqa.gwentdecktracker.util.CardComparator;

/**
 * Created by ben on 21/9/17.
 */

public abstract class BaseCardListAdapter extends BaseAdapter {
    protected Context context;
    protected List<Card> cardList;

    public BaseCardListAdapter(Context context, List<Card> cardList) {
        this.context = context;
        this.cardList = cardList;
    }

    public void setCardList(List<Card> cardList) {
        this.cardList = cardList;
    }

    @Override
    public int getCount() {
        return cardList == null ? 0 : cardList.size();
    }

    @Override
    public Object getItem(int position) {
        return cardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @DrawableRes
    protected int getColourForCard(Card card) {
        if (card.getGroup() == null) {
            return R.color.rarityUncommon;
        }

        switch(card.getGroup().getName()){
            case LEADER : return R.color.rarityGold;
            case GOLD : return R.color.rarityGold;
            case SILVER: return R.color.raritySilver;
            case BRONZE: return R.color.rarityBronze;
            default: return R.color.rarityUncommon;
        }
    }
}

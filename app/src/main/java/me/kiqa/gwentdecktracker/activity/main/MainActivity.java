package me.kiqa.gwentdecktracker.activity.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.kiqa.gwentdecktracker.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}

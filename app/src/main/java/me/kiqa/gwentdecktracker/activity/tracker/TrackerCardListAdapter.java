package me.kiqa.gwentdecktracker.activity.tracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import me.kiqa.gwentdecktracker.R;
import me.kiqa.gwentdecktracker.activity.common.BaseCardListAdapter;
import me.kiqa.gwentdecktracker.model.json.Card;

/**
 * Created by Ben on 17/09/2017.
 */

public class TrackerCardListAdapter extends BaseCardListAdapter {

    public TrackerCardListAdapter(Context context, List<Card> cardList) {
        super(context, cardList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get the custom view from resources and inflate it
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.card_list_row, parent, false);
        // get the card at this position
        //Log.i("CardAdapter", "getting item via view: " + position);
        Card card = cardList.get(position);

        // set up the fields in the view
        TextView colourStripe = (TextView) rowView.findViewById(R.id.rowColourStripe);
        TextView strength = (TextView) rowView.findViewById(R.id.rowStrength);
        TextView title = (TextView) rowView.findViewById(R.id.rowTitle);
        TextView info = (TextView) rowView.findViewById(R.id.rowInfo);
        Button handButton = (Button) rowView.findViewById(R.id.rowLeftButton);
        Button graveyardButton = (Button) rowView.findViewById(R.id.rowMiddleButton);
        Button deckButton = (Button) rowView.findViewById(R.id.rowRightButton);

        // set the position in list to the button tag to identify
        // which row was clicked within the click handler
        graveyardButton.setTag(position);
        deckButton.setTag(position);
        handButton.setTag(position);

        // click handlers for each button
        graveyardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card selected = cardList.remove((int) view.getTag());
                TrackerActivity context = ((TrackerActivity) view.getContext());
                context.getGraveyardCardList().getCardList().add(selected);
                context.getGraveyardCardList().sortList();
                context.getAdapter().notifyDataSetChanged();
            }
        });

        deckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card selected = cardList.remove((int) view.getTag());
                TrackerActivity context = ((TrackerActivity) view.getContext());
                context.getDeckCardList().getCardList().add(selected);
                context.getDeckCardList().sortList();
                context.getAdapter().notifyDataSetChanged();
            }
        });

        handButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Card selected = cardList.remove((int) view.getTag());
                TrackerActivity context = ((TrackerActivity) view.getContext());
                context.getHandCardList().getCardList().add(selected);
                context.getHandCardList().sortList();
                context.getAdapter().notifyDataSetChanged();
            }
        });

        // map values from card to row view
        colourStripe.setBackgroundResource(getColourForCard(card));
        strength.setText(card.getStrength() != null ? card.getStrength().toString() : "*");
        title.setText(card.getName());
        info.setText(card.getInfo() != null ? card.getInfo() : "");
        return rowView;
    }

}

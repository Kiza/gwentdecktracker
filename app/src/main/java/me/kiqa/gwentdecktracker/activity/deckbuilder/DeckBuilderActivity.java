package me.kiqa.gwentdecktracker.activity.deckbuilder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import me.kiqa.gwentdecktracker.R;
import me.kiqa.gwentdecktracker.constant.CardListTypeEnum;
import me.kiqa.gwentdecktracker.model.CardList;

public class DeckBuilderActivity extends AppCompatActivity {
    private CardList builderList = new CardList(CardListTypeEnum.BUILDER, "Builder");
    private CardList deckList = new CardList(CardListTypeEnum.DECK,"BuilderDeck");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck_builder);
    }
}

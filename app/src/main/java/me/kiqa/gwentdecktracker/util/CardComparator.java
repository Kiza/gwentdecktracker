package me.kiqa.gwentdecktracker.util;

import java.util.Comparator;

import me.kiqa.gwentdecktracker.model.json.Card;

/**
 * Created by ben on 21/9/17.
 */

public class CardComparator implements Comparator<Card>{

    @Override
    public int compare(Card a, Card b) {
        return compareCardType(a,b);

    }

    private int compareCardType(Card a, Card b) {
        if (a.getGroup().getName() == b.getGroup().getName()){
            return compareCardStrength(a,b);
        }

        return a.getGroup().getName().getSort() - b.getGroup().getName().getSort();
    }

    private int compareCardStrength(Card a, Card b) {
        int aStrength = a.getStrength() != null ? a.getStrength() : 9999;
        int bStrength = b.getStrength() != null ? b.getStrength() : 9999;

        if (aStrength == bStrength){
            return compareCardName(a, b);
        }

        return bStrength - aStrength;
    }

    private int compareCardName(Card a, Card b) {
        return a.getName().compareToIgnoreCase(b.getName());
    }

}

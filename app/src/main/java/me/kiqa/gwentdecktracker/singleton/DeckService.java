package me.kiqa.gwentdecktracker.singleton;

import java.util.List;

import me.kiqa.gwentdecktracker.model.json.Card;

/**
 * Created by ben on 20/9/17.
 */

public class DeckService {
    private static List<Card> deck;
    private static final DeckService deckService;

    static {
        deckService = new DeckService();
    }

    private DeckService(){}

    public static DeckService getInstance(){
        return deckService;
    }
}

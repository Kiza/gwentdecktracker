package me.kiqa.gwentdecktracker.constant;

/**
 * Created by ben on 21/9/17.
 */

public enum CardListTypeEnum {
    DECK,
    BOARD,
    GRAVEYARD,
    HAND,
    BUILDER
}

package me.kiqa.gwentdecktracker.constant;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Ben on 18/09/2017.
 */

public enum CardTypeEnum {
    @JsonProperty("Leader")
    LEADER("Leader",0),
    @JsonProperty("Gold")
    GOLD("Gold",1),
    @JsonProperty("Silver")
    SILVER("Silver",2),
    @JsonProperty("Bronze")
    BRONZE("Bronze",3);

    private final String text;
    private final int sort;

    private CardTypeEnum(final String text, final int sort) {
        this.text = text;
        this.sort = sort;
    }

    public String getText(){
        return text;
    }
    public int getSort(){
        return sort;
    }
}